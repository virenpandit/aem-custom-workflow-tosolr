package com.wyn.myportal.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.Value;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Created by C13774 on 11/17/2014.
 */
public class MyPortalAnalyticsHelper {

    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(MyPortalAnalyticsHelper.class);

    public static Map<String, String> updateUserRole(Map<String,String> datamap){
        String res = "";
        if(datamap != null && datamap.containsKey("franchiseeroles")){
           String value = datamap.get("franchiseeroles");
            if (value == null || value.equalsIgnoreCase("NOT_FOUND")) {
                res = "internal";
                datamap.put("status", res);
                datamap.put("role", datamap.get("businessroles"));
            } else {
                res = "franchisee";
                datamap.put("status", res);
                datamap.put("role", value);
            }
        }
        return datamap;

    }


    public static Map<String, String> updateReviewedDate(Map<String,String> datamap){
        String res = "";
        if(datamap != null){
            datamap.put("revDate", MyPortalUtil.findAPFormattedDate(new Date()));
        }
        return datamap;

    }


    public static Map<String, String> updateBrandMattersData(Map<String,String> datamap, Node pageNode,ResourceResolver resourceResolver){
        Property propCategory = null;
        String category = "category";

        if(datamap != null && pageNode!= null ){
             try{
                if(pageNode.hasProperty("displayDate")){
                    datamap.put("publishedDate", MyPortalUtil.findAPFormattedDate(pageNode.getProperty("displayDate").getValue().getDate().getTime()));
                }
                if(pageNode.hasProperty(category)){
                    ArrayList<String> list = new ArrayList<String>();
                    propCategory = pageNode.getProperty(category);
                    if(propCategory.isMultiple()) {
                        Value[] categoryValue = propCategory.getValues();
                        if (!StringUtils.isEmpty(category)
                                && categoryValue.length > 0) {

                            for (int i = 0; i < categoryValue.length; i++) {
                                list.add(MyPortalUtil.getTagTitle(categoryValue[i].getString(), resourceResolver, ""));
                            }

                        }
                    }
                    else{

                        String value = (String) propCategory.getValue().getString();
                        if (!StringUtils.isEmpty(category)
                                && !StringUtils.isEmpty(value)) {
                            list.add(MyPortalUtil.getTagTitle(value, resourceResolver, ""));
                        }
                    }

                    datamap.put("category",StringUtils.join(list,","));
                    //System.out.println(list.toArray().toString() + " --- " + StringUtils.join(list,","));

                }


            }catch (Exception e ){}

        }
        return datamap;

    }


    public static Map<String, String> getHeaders(HttpServletRequest request, Node pageNode, ResourceResolver resourceResolver){
        String res = "";
        Map<String,String> datamap = new HashMap<String, String>();
        try{
            Enumeration e = request.getHeaderNames();
            while (e.hasMoreElements()) {
                String name = (String) e.nextElement();
                Enumeration vs = request.getHeaders(name);
                StringBuilder b = new StringBuilder();
                while (vs.hasMoreElements()) {
                    if (b.length() > 0) {
                        b.append(",");
                    }
                    b.append(vs.nextElement());
                }

                log.info(name + " ---> " + b.toString());
                datamap.put(name,b.toString());
            }
        }catch (Exception e){
            log.error("Error in parsing header data");
        }

        //Get the correct roles
        datamap = updateUserRole(datamap);
        //Get the reviewed date
        datamap = updateReviewedDate(datamap);

        //Get the brand matters data
        datamap = updateBrandMattersData(datamap, pageNode, resourceResolver);


        return datamap;

    }



}
